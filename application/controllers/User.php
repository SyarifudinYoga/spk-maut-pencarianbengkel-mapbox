<?php

class User extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

    function index(){
		$this->load->view('v_login');
    }

    function auth(){
        $email    = $this->input->post('email',TRUE);
        $password = $this->input->post('password',TRUE);
        $validate = $this->user_model->validate($email,$password);
        if($validate->num_rows() > 0){
            $data  = $validate->row_array();
            $nama  = $data['nama'];
            $email = $data['email'];
            $level = $data['level'];
            $sesdata = array(
                'username'  => $nama,
                'email'     => $email,
                'level'     => $level,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($sesdata);
            //Akses Login Kesekertariatan
            if($level === 'Admin'){
                redirect('user/Admin');
            }
        }else{
            echo $this->session->set_flashdata('msg','Email Atau Password Tidak Sesuai');
            redirect('user');
        }   
    }

    function Admin(){
        $data['lokasi'] = $this->user_model->tampil_lokasi();
        $this->load->view('Admin/dashboard_admin',$data);
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('user');
    }
}