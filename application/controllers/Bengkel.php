<?php

class Bengkel extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('bengkel_model');
	}

    function index(){
		$this->load->view('v_bengkel');
	}
	
	//=========================================================================================================Bengkel
	//tampilan awal bengkel
	function kelola_bengkel(){
		$data['bengkel'] = $this->bengkel_model->tampil_bengkel();
		$this->load->view('Admin/kelola_bengkel',$data);
	}
	//lempar ke form
	function form_tambah_bengkel(){
		$this->load->view('Admin/form_tambah_bengkel');
	}
	//eksekusi
	function tambah_bengkel(){
		$nama_bengkel = $this->input->post('nama_bengkel');
		$nama_pemilik = $this->input->post('nama_pemilik');
		$no_telp = $this->input->post('no_telp');
		$jumlah_montir = $this->input->post('jumlah_montir');
		$alamat = $this->input->post('alamat');
		$data = array (
			'nama_bengkel' => $nama_bengkel,
			'nama_pemilik' => $nama_pemilik,
			'no_telp' => $no_telp,
			'jumlah_montir' => $jumlah_montir,
			'alamat' => $alamat
		);
		$data1 = array (
			'nama_bengkel' => $nama_bengkel
		);
		if($data == null) {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-danger">
			  <p>Data Bengkel Gagal Ditambahkan.</p>
			</div>');
			$this->kelola_bengkel();
		  } else {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <p>Data <strong>Bengkel</strong> Berhasil Ditambahkan </p>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>');
		  $this->bengkel_model->tambah_bengkel('tbl_bengkel', $data);
		  $this->bengkel_model->tambah_penilaian_bengkel('tbl_penilaian', $data1);
		  $this->kelola_bengkel();
		  };
	}
	//lempar ke form edit
	function form_edit_bengkel(){
		$nama_bengkel = $this->input->post('nama_bengkel');
    	$data['bengkel'] = $this->bengkel_model->get_nama_bengkel($nama_bengkel);
      	$this->load->view('Admin/form_edit_bengkel', $data);
	}
	//lihat
	function form_lihat_bengkel(){
		$nama_bengkel = $this->input->post('nama_bengkel');
    	$data['bengkel'] = $this->bengkel_model->get_nama_bengkel($nama_bengkel);
      	$this->load->view('Admin/form_lihat_bengkel', $data);
	}
	//eksekusi edit
	function ubah_bengkel(){
		$nama_bengkel = $this->input->post('nama_bengkel');
		$nama_pemilik = $this->input->post('nama_pemilik');
		$no_telp = $this->input->post('no_telp');
		$jumlah_montir = $this->input->post('jumlah_montir');
		$alamat = $this->input->post('alamat');
		$data = array (
			'nama_bengkel' => $nama_bengkel,
			'nama_pemilik' => $nama_pemilik,
			'no_telp' => $no_telp,
			'jumlah_montir' => $jumlah_montir,
			'alamat' => $alamat
		);
		if($data == null) {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-danger">
			  <p>Data Bengkel Gagal Diubah.</p>
			</div>');
			$this->kelola_bengkel();
		  } else {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <p>Data <strong>Bengkel</strong> Berhasil Diubah </p>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>');
		  $this->bengkel_model->ubah_bengkel('tbl_bengkel', $data,$nama_bengkel);
		  $this->kelola_bengkel();
		  };
	}
	//frontend user
	function cari_bengkel(){
		$data['bengkel']= $this->bengkel_model->cari_bengkel();
      	$this->load->view('v_cari_bengkel', $data);
	}
	
	//=========================================================================================================Sparepart
	function kelola_sparepart(){
		$data['sparepart'] = $this->bengkel_model->tampil_sparepart();
		$this->load->view('Admin/kelola_sparepart',$data);
	}

	function form_tambah_sparepart(){
		$data['bengkel'] = $this->bengkel_model->tampil_bengkel();
		$this->load->view('Admin/form_tambah_sparepart', $data);
	}

	function tambah_sparepart(){
		$produk_id = $this->input->post('produk_id');
		$nama_bengkel = $this->input->post('nama_bengkel');
		$produk_nama = $this->input->post('produk_nama');
		$produk_harga = $this->input->post('produk_harga');
		//get Pdf
        $config['upload_path'] = './sparepart';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';  //2MB max
        $config['file_name'] = $_FILES['produk_image']['name'];
        $this->upload->initialize($config);
        if (!empty($_FILES['produk_image']['name'])) {
          if ( $this->upload->do_upload('produk_image') ) {
            $file = $this->upload->data();
              $data = array(
                'produk_id' => $produk_id,
                'nama_bengkel' => $nama_bengkel,
                'produk_nama' => $produk_nama,
                'produk_harga' => $produk_harga,
                'produk_image' => $file['file_name'],
              );
              if($data == null) {
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-danger">
                            <h4>Oppss</h4>
                            <p>Tidak ada data dinput.</p>
                        </div>');
				$this->kelola_sparepart();
              }else{
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>Data <strong>Sparepart</strong> Berhasil Ditambahkan </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>');
                        $this->bengkel_model->tambah_sparepart($data);
                	$this->kelola_sparepart();
            };
          }else {
              die("gagal upload");
          }
        }else {
          echo "tidak masuk";
        }
	}

	function form_edit_sparepart(){
		$produk_id = $this->input->post('produk_id');
      	$data['bengkel'] = $this->bengkel_model->tampil_bengkel();
      	$data['sparepart'] = $this->bengkel_model->get_produk_id($produk_id);
      	$this->load->view('Admin/form_edit_sparepart', $data);
	}

	function form_lihat_sparepart(){
		$produk_id = $this->input->post('produk_id');
      	//$data['bengkel'] = $this->bengkel_model->tampil_bengkel();
      	$data['sparepart'] = $this->bengkel_model->get_produk_id($produk_id);
      	$this->load->view('Admin/form_lihat_sparepart', $data);
	}

	function ubah_sparepart(){
		$produk_id = $this->input->post('produk_id');
		$nama_bengkel = $this->input->post('nama_bengkel');
		$produk_nama = $this->input->post('produk_nama');
		$produk_harga = $this->input->post('produk_harga');
		//get Pdf
        $config['upload_path'] = './sparepart';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';  //2MB max
        $config['file_name'] = $_FILES['produk_image']['name'];
        $this->upload->initialize($config);
        if (!empty($_FILES['produk_image']['name'])) {
          if ( $this->upload->do_upload('produk_image') ) {
            $file = $this->upload->data();
              $data = array(
                'produk_id' => $produk_id,
                'nama_bengkel' => $nama_bengkel,
                'produk_nama' => $produk_nama,
                'produk_harga' => $produk_harga,
                'produk_image' => $file['file_name'],
              );
              if($data == null) {
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-danger">
                            <h4>Oppss</h4>
                            <p>Tidak ada data dinput.</p>
                        </div>');
				$this->kelola_sparepart();
              }else{
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>Data <strong>Sparepart</strong> Berhasil Diubah </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>');
                $this->bengkel_model->ubah_sparepart('tbl_produk',$data,$produk_id);
                $this->kelola_sparepart();
            };
          }else {
              die("gagal upload");
          }
        }else {
          echo "tidak masuk";
        }
	}

	public function hapus_sparepart(){
		$produk_id =$this->input->post('produk_id');
		if($produk_id == null) {
		  $this->session->set_flashdata('hps',
			'<div class="alert alert-danger">
			<p>Data <strong>Sparepart </strong> Gagal Dihapus </p>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
			</div>');
			$this->kelola_sparepart();
		}else{
		  $this->session->set_flashdata('hps',
			'<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <p>Data <strong>Sparepart </strong> Berhasil Dihapus </p>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>');
			$this->bengkel_model->hapus_sparepart($produk_id);
			$this->kelola_sparepart();
		};
	  }
	
}