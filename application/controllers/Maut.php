<?php

class Maut extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('maut_model');
	}

    function kelola_nilai_bengkel(){
		$data['nilai'] = $this->maut_model->tampil_nilai();
		$this->load->view('Admin/kelola_nilai_bengkel',$data);
	}
	
	function kelola_hitung_bengkel(){
		$data['hitung'] = $this->maut_model->tampil_hitung();
		$this->load->view('Admin/kelola_hitung_bengkel',$data);
	}

	function form_tambah_penilaian(){
		$data['bengkel'] = $this->maut_model->tampil_bengkel_penilaian();
		$this->load->view('Admin/form_tambah_penilaian',$data);
	}

	function form_edit_penilaian(){
		$id_penilaian = $this->input->post('id_penilaian');
		$data['nilai'] = $this->maut_model->get_id_penilaian($id_penilaian);
		$this->load->view('Admin/form_edit_penilaian',$data);
	}

	function form_lihat_penilaian(){
		$id_penilaian = $this->input->post('id_penilaian');
		$data['nilai'] = $this->maut_model->get_id_penilaian($id_penilaian);
		$this->load->view('Admin/form_lihat_penilaian',$data);
	}

	function tambah_penilaian(){
		$nama_bengkel = $this->input->post('nama_bengkel');
		$sparepart = $this->input->post('sparepart');
		$waktu_operasional = $this->input->post('waktu_operasional');
		$fasilitas = $this->input->post('fasilitas');
		$jumlah_mekanik = $this->input->post('jumlah_mekanik');
		$rating = $this->input->post('rating');
		$estimasi_waktu_ringan = $this->input->post('estimasi_waktu_ringan');
		$estimasi_waktu_sedang = $this->input->post('estimasi_waktu_sedang');
		$estimasi_waktu_berat = $this->input->post('estimasi_waktu_berat');
		$kisaran_harga_ringan = $this->input->post('kisaran_harga_ringan');
		$kisaran_harga_sedang = $this->input->post('kisaran_harga_sedang');
		$kisaran_harga_berat = $this->input->post('kisaran_harga_berat');
		$data = array (
			'nama_bengkel' => $nama_bengkel,
			'sparepart' => $sparepart,
			'waktu_operasional' => $waktu_operasional,
			'fasilitas' => $fasilitas,
			'jumlah_mekanik' => $jumlah_mekanik,
			'rating' => $rating,
			'estimasi_waktu_ringan' => $estimasi_waktu_ringan,
			'estimasi_waktu_sedang' => $estimasi_waktu_sedang,
			'estimasi_waktu_berat' => $estimasi_waktu_berat,
			'kisaran_harga_ringan' => $kisaran_harga_ringan,
			'kisaran_harga_sedang' => $kisaran_harga_sedang,
			'kisaran_harga_berat' => $kisaran_harga_berat
		);
		if($data == null) {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-danger">
			  <p>Data Penilaian Bengkel Gagal Ditambahkan.</p>
			</div>');
			$this->kelola_nilai_bengkel();
		  } else {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <p>Data <strong>Penilaian Bengkel</strong> Berhasil Ditambahkan </p>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>');
		  $this->maut_model->tambah_penilaian('tbl_penilaian', $data);
		  $this->kelola_nilai_bengkel();
		  };
	}

	function edit_penilaian(){
		$id_penilaian = $this->input->post('id_penilaian');
		$nama_bengkel = $this->input->post('nama_bengkel');
		$sparepart = $this->input->post('sparepart');
		$waktu_operasional = $this->input->post('waktu_operasional');
		$fasilitas = $this->input->post('fasilitas');
		$jumlah_mekanik = $this->input->post('jumlah_mekanik');
		$rating = $this->input->post('rating');
		$estimasi_waktu_ringan = $this->input->post('estimasi_waktu_ringan');
		$estimasi_waktu_sedang = $this->input->post('estimasi_waktu_sedang');
		$estimasi_waktu_berat = $this->input->post('estimasi_waktu_berat');
		$kisaran_harga_ringan = $this->input->post('kisaran_harga_ringan');
		$kisaran_harga_sedang = $this->input->post('kisaran_harga_sedang');
		$kisaran_harga_berat = $this->input->post('kisaran_harga_berat');
		$data = array (
			'id_penilaian' =>$id_penilaian,
			'nama_bengkel' => $nama_bengkel,
			'sparepart' => $sparepart,
			'waktu_operasional' => $waktu_operasional,
			'fasilitas' => $fasilitas,
			'jumlah_mekanik' => $jumlah_mekanik,
			'rating' => $rating,
			'estimasi_waktu_ringan' => $estimasi_waktu_ringan,
			'estimasi_waktu_sedang' => $estimasi_waktu_sedang,
			'estimasi_waktu_berat' => $estimasi_waktu_berat,
			'kisaran_harga_ringan' => $kisaran_harga_ringan,
			'kisaran_harga_sedang' => $kisaran_harga_sedang,
			'kisaran_harga_berat' => $kisaran_harga_berat
		);
		if($data == null) {
			$this->session->set_flashdata('edt',
			'<div class="alert alert-danger">
			  <p>Data Penilaian Bengkel Gagal Diubah.</p>
			</div>');
			$this->kelola_nilai_bengkel();
		  } else {
			$this->session->set_flashdata('edt',
			'<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <p>Data <strong>Penilaian Bengkel</strong> Berhasil Diubah </p>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>');
		  $this->maut_model->edit_penilaian('tbl_penilaian', $data, $id_penilaian);
		  $this->kelola_nilai_bengkel();
		  };
	}

	function hitung_penilaian(){
		$id_penilaian = $this->input->post('id_penilaian');
		$data['nilai'] = $this->maut_model->tampil_nilai();
    	$data['maut'] = $this->maut_model->get_id_penilaian($id_penilaian);
    	$data['max'] = $this->maut_model->max();
    	$data['min'] = $this->maut_model->min();
		$this->load->view('Admin/form_hitung_penilaian',$data);
	}

	function hitung_maut(){
		$id_penilaian = $this->input->post('id_penilaian');
		//kriteria
		$nama_bengkel = $this->input->post('nama_bengkel');
		$sparepart = $this->input->post('sparepart');
		$waktu_operasional = $this->input->post('waktu_operasional');
		$fasilitas = $this->input->post('fasilitas');
		$jumlah_mekanik = $this->input->post('jumlah_mekanik');
		$rating = $this->input->post('rating');
		$estimasi_waktu_ringan = $this->input->post('estimasi_waktu_ringan');
		$estimasi_waktu_sedang = $this->input->post('estimasi_waktu_sedang');
		$estimasi_waktu_berat = $this->input->post('estimasi_waktu_berat');
		$kisaran_harga_ringan = $this->input->post('kisaran_harga_ringan');
		$kisaran_harga_sedang = $this->input->post('kisaran_harga_sedang');
		$kisaran_harga_berat = $this->input->post('kisaran_harga_berat');
		//Nilai Max
		$max_sparepart = $this->input->post('max_sparepart');
		$max_waktu_operasional = $this->input->post('max_waktu_operasional');
		$max_fasilitas = $this->input->post('max_fasilitas');
		$max_jumlah_mekanik = $this->input->post('max_jumlah_mekanik');
		$max_rating = $this->input->post('max_rating');
		$max_estimasi_waktu_ringan = $this->input->post('max_estimasi_waktu_ringan');
		$max_estimasi_waktu_sedang = $this->input->post('max_estimasi_waktu_sedang');
		$max_estimasi_waktu_berat = $this->input->post('max_estimasi_waktu_berat');
		$max_kisaran_harga_ringan = $this->input->post('max_kisaran_harga_ringan');
		$max_kisaran_harga_sedang = $this->input->post('max_kisaran_harga_sedang');
		$max_kisaran_harga_berat = $this->input->post('max_kisaran_harga_berat');
		//Nilai Min
		$min_sparepart = $this->input->post('min_sparepart');
		$min_waktu_operasional = $this->input->post('min_waktu_operasional');
		$min_fasilitas = $this->input->post('min_fasilitas');
		$min_jumlah_mekanik = $this->input->post('min_jumlah_mekanik');
		$min_rating = $this->input->post('min_rating');
		$min_estimasi_waktu_ringan = $this->input->post('min_estimasi_waktu_ringan');
		$min_estimasi_waktu_sedang = $this->input->post('min_estimasi_waktu_sedang');
		$min_estimasi_waktu_berat = $this->input->post('min_estimasi_waktu_berat');
		$min_kisaran_harga_ringan = $this->input->post('min_kisaran_harga_ringan');
		$min_kisaran_harga_sedang = $this->input->post('min_kisaran_harga_sedang');
		$min_kisaran_harga_berat = $this->input->post('min_kisaran_harga_berat');
		//bobot
		$bobot_sparepart = 0.10;
		$bobot_waktu_operasional = 0.25;
		$bobot_fasilitas = 0.15;
		$bobot_jumlah_mekanik = 0.10;
		$bobot_rating = 0.10;
		$bobot_estimasi_waktu_ringan = 0.03;
		$bobot_estimasi_waktu_sedang = 0.03;
		$bobot_estimasi_waktu_berat = 0.03;
		$bobot_kisaran_harga_ringan = 0.06;
		$bobot_kisaran_harga_sedang = 0.06;
		$bobot_kisaran_harga_berat = 0.06;

		//Hitung Selisih Max - Min
		$s1 = $max_sparepart-$min_sparepart;
		$s2 = $max_waktu_operasional-$min_waktu_operasional;
		$s3 = $max_fasilitas-$min_fasilitas;
		$s4 = $max_jumlah_mekanik-$min_jumlah_mekanik;
		$s5 = $max_rating-$min_rating;
		$s6 = $max_estimasi_waktu_ringan-$min_estimasi_waktu_ringan;
		$s7 = $max_estimasi_waktu_sedang-$min_estimasi_waktu_sedang;
		$s8 = $max_estimasi_waktu_berat-$min_estimasi_waktu_berat;
		$s9 = $max_kisaran_harga_ringan-$min_kisaran_harga_ringan;
		$s10 = $max_kisaran_harga_sedang-$min_kisaran_harga_sedang;
		$s11 = $max_kisaran_harga_berat-$min_kisaran_harga_berat;

		$data= array();
    $index=0;
      foreach($id_penilaian as $idp){
    //Kriteria-Nilai Min (k = kriteria) 
    $k1 = $sparepart[$index] - $min_sparepart;
    $k2 = $waktu_operasional[$index] - $min_waktu_operasional;
    $k3 = $fasilitas[$index] - $min_fasilitas;
    $k4 = $jumlah_mekanik[$index] - $min_jumlah_mekanik;
    $k5 = $rating[$index] - $min_rating;
	$k6 = $estimasi_waktu_ringan[$index] - $min_estimasi_waktu_ringan;
	$k7 = $estimasi_waktu_sedang[$index] - $min_estimasi_waktu_sedang;
	$k8 = $estimasi_waktu_berat[$index] - $min_estimasi_waktu_berat;
	$k9 = $kisaran_harga_ringan[$index] - $min_kisaran_harga_ringan;
	$k10 = $kisaran_harga_sedang[$index] - $min_kisaran_harga_sedang;
	$k11 = $kisaran_harga_berat[$index] - $min_kisaran_harga_berat;
    
    //Normalisasi (N = Normmalisasi)
    $N1= $k1/$s1;
    $N2= $k2/$s2;
    $N3= $k3/$s3;
    $N4= $k4/$s4;
    $N5= $k5/$s5;
    $N6= $k6/$s6;
	$N7= $k7/$s7;
	$N8= $k8/$s8;
	$N9= $k9/$s9;
	$N10= $k10/$s10;
	$N11= $k11/$s11;
    
    //MAUT (A = Alternatif)
    $A1 = $N1*$bobot_sparepart ;
    $A2 = $N2*$bobot_waktu_operasional ;
    $A3 = $N3*$bobot_fasilitas ;
    $A4 = $N4*$bobot_jumlah_mekanik;
    $A5 = $N5*$bobot_rating ;
	$A6 = $N6*$bobot_estimasi_waktu_ringan ;
	$A7 = $N7*$bobot_estimasi_waktu_sedang ;
	$A8 = $N8*$bobot_estimasi_waktu_berat ;
	$A9 = $N9*$bobot_kisaran_harga_ringan ;
	$A10 = $N10*$bobot_kisaran_harga_sedang ;
	$A11 = $N11*$bobot_kisaran_harga_berat ;
	
    $hasilHitung = $A1+$A2+$A3+$A4+$A5+$A6+$A7+$A8+$A9+$A10+$A11;

	array_push($data,array(
		'id_maut' => $idp,
        'nama_bengkel' => $nama_bengkel[$index],
        'normalisasi_sparepart' => $N1,
        'normalisasi_waktu_operasional' => $N2,
        'normalisasi_fasilitas' => $N3,
        'normalisasi_jumlah_mekanik' => $N4,
        'normalisasi_rating' => $N5,
		'normalisasi_estimasi_waktu_ringan' => $N6 ,
		'normalisasi_estimasi_waktu_sedang' => $N7 ,
		'normalisasi_estimasi_waktu_berat' => $N8 ,
        'normalisasi_kisaran_harga_ringan' => $N9 ,
		'normalisasi_kisaran_harga_sedang' => $N10 ,
		'normalisasi_kisaran_harga_berat' => $N11 ,

        'hasil_sparepart' => $A1,
        'hasil_waktu_operasional' => $A2,
        'hasil_fasilitas' => $A3,
        'hasil_jumlah_mekanik' => $A4,
        'hasil_rating' => $A5,
		'hasil_estimasi_waktu_ringan' => $A6 ,
		'hasil_estimasi_waktu_sedang' => $A7 ,
		'hasil_estimasi_waktu_berat' => $A8 ,
		'hasil_kisaran_harga_ringan' => $A9 ,
		'hasil_kisaran_harga_sedang' => $A10 ,
		'hasil_kisaran_harga_berat' => $A11 ,
        
        'nilai_akhir' => $hasilHitung
    ));
    $index++;
  }
  if($data == null) {
    $this->session->set_flashdata('msg',
      '<div class="alert alert-danger">
      <p>Data <strong>Perhitungan Bengkel</strong> Gagal Ditambahkan </p>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>');
      $this->kelola_hitung_bengkel();
  }else{
    $this->session->set_flashdata('msg',
      '<div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>Data <strong>Perhitungan Penilaian Bengkel</strong> Berhasil Ditambahkan </p>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>');
  $this->maut_model->hitung_maut('tbl_maut', $data);
  $this->kelola_hitung_bengkel();  
  };
  }
	
  function hapus_penilaian(){
	$this->db->empty_table('tbl_maut');
	$this->kelola_hitung_bengkel(); 
  }
    
}