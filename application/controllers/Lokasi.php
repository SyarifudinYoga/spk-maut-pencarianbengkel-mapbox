<?php

class Lokasi extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('lokasi_model');
	}

    function index(){
		$data['lokasi'] = $this->lokasi_model->tampilLokasi();
		$this->load->view('v_peta',$data);
	}
	
	function kelola_lokasi(){
		$data['lokasi'] = $this->lokasi_model->tampil_lokasi();
		$this->load->view('Admin/kelola_lokasi',$data);
	}

	function form_tambah_lokasi(){
		$data['bengkel'] = $this->lokasi_model->tampil_bengkel();
		$this->load->view('Admin/form_tambah_lokasi',$data);
	}

	function tambah_lokasi(){
		$id_lokasi = $this->input->post('id_lokasi');
		$nama_bengkel = $this->input->post('nama_bengkel');
		$latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');
		$data = array(
			'id_lokasi' => $id_lokasi,
			'nama_bengkel' => $nama_bengkel,
			'latitude' => $latitude,
			'longitude' => $longitude
		);
		if($data == null) {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-danger">
			  <p>Data Lokasi Gagal Ditambahkan.</p>
			</div>');
			$this->kelola_lokasi();
		  } else {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <p>Data <strong>Lokasi</strong> Berhasil Ditambahkan </p>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>');
		  $this->lokasi_model->tambah_lokasi('tbl_lokasi', $data);
		  $this->kelola_lokasi();
		  };
	}

	function form_edit_lokasi(){
		$id_lokasi = $this->input->post('id_lokasi');
		$data['bengkel'] = $this->lokasi_model->tampil_bengkel();
		$data['lokasi'] = $this->lokasi_model->get_id_lokasi($id_lokasi);
		$this->load->view('Admin/form_edit_lokasi',$data);
	}

	function form_lihat_lokasi(){
		$id_lokasi = $this->input->post('id_lokasi');
		$data['bengkel'] = $this->lokasi_model->tampil_bengkel();
		$data['lokasi'] = $this->lokasi_model->get_id_lokasi($id_lokasi);
		$this->load->view('Admin/form_lihat_lokasi',$data);
	}

	function ubah_lokasi(){
		$id_lokasi = $this->input->post('id_lokasi');
		$nama_bengkel = $this->input->post('nama_bengkel');
		$latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');
		$data = array(
			'id_lokasi' => $id_lokasi,
			'nama_bengkel' => $nama_bengkel,
			'latitude' => $latitude,
			'longitude' => $longitude
		);
		if($data == null) {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-danger">
			  <p>Data Lokasi Gagal Diedit.</p>
			</div>');
			$this->kelola_lokasi();
		  } else {
			$this->session->set_flashdata('msg',
			'<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <p>Data <strong>Lokasi</strong> Berhasil Diedit </p>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>');
		  $this->lokasi_model->ubah_lokasi('tbl_lokasi', $data, $id_lokasi);
		  $this->kelola_lokasi();
		  };
	}
	
	
}