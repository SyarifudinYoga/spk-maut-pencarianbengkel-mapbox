<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/bootstrap/dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/themify-icons/css/themify-icons.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/flag-icon-css/css/flag-icon.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/selectFX/css/cs-skin-elastic.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/assets/css/style.css');?>">
    <link href="<?php echo base_url('template/assets/css/font.css');?>" rel="stylesheet" type="text/css">
    <!-- Validasi dengan JavaScript -->
<script language="javascript">
	function getkey(e)
	{
		if (window.event)
   		return window.event.keyCode;
		else if (e)
   		return e.which;
		else
   		return null;
	}
	function goodchars(e, goods, field)
	{
	var key, keychar;
	key = getkey(e);
		if (key == null) return true;
		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();
		goods = goods.toLowerCase();
	// check goodkeys
	if (goods.indexOf(keychar) != -1)
			return true;
	// control keys
	if ( key==null || key==0 || key==8 || key==9 || key==27 )
		return true;		
	if (key == 13) {
			var i;
			for (i = 0; i < field.form.elements.length; i++)
					if (field == field.form.elements[i])
							break;
			i = (i + 1) % field.form.elements.length;
			field.form.elements[i].focus();
			return false;
			};
	// else return false
	return false;
	}
    </script>
</head>
<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a href="<?php echo base_url(); ?>index.php/user/admin"><br>
                <img src="<?php echo base_url('assets/spk.png');?>" style="width:180px";>
                </a>
            </div>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                <h3 class="menu-title"><center>KABUPATEN BANDUNG BARAT</center></h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Dashboard</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-calendar-check-o"></i><a href="<?php echo base_url(); ?>index.php/user/Admin">Peta Bengkel</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar"></i>Bengkel</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/bengkel/kelola_bengkel">Kelola Bengkel</a></li>
                            <!--<li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php //echo base_url(); ?>index.php/bengkel/kelola_sparepart">Kelola Sparepart</a></li>-->
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/lokasi/kelola_lokasi">Kelola Lokasi</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-sticky-note"></i>Penilaian</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_nilai_bengkel">Kelola Penilaian Bengkel</a></li>
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_hitung_bengkel">Kelola Perhitungan Bengkel</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="header-menu">
            <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                    <a class="nav-link active" href="#">Halaman <b><?php echo $this->session->userdata('username');?></b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo site_url('login/logout');?>">Logout</a>
                </li>
                </ul>
            </div>
        </header><!-- /header -->
        <!-- Header-->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Edit Bengkel</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card">
            <form action="<?php echo base_url(); ?>index.php/bengkel/ubah_bengkel" method="POST">
                <div class="card-header"><strong>Data</strong><small> Bengkel</small></div>
                    <div class="card-body card-block">
                        <div class="form-group"><label for="company" class=" form-control-label">Nama Bengkel</label>
                        <input type="hidden" name="nama_bengkel" placeholder="Masukkan Nama Bengkel" class="form-control" value="<?php echo $bengkel[0]->nama_bengkel;?>">
                        <input type="text" name="nama_bengkel" placeholder="Masukkan Nama Bengkel" class="form-control" value="<?php echo $bengkel[0]->nama_bengkel;?>" readonly>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Nama Pemilik</label>
                        <input type="hidden" name="nama_pemilik" placeholder="Masukkan Nama Pemilik" class="form-control" value="<?php echo $bengkel[0]->nama_pemilik;?>">
                        <input type="text" name="nama_pemilik" placeholder="Masukkan Nama Pemilik" class="form-control" value="<?php echo $bengkel[0]->nama_pemilik;?>" onKeyPress="return goodchars(event,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ,.',this)" maxlength="50" required>
                        </div>
                        <div class="form-group"><label for="street" class=" form-control-label">Nomor Telepon</label>
                        <input type="hidden" name="no_telp" placeholder="Masukkan No Telpon" class="form-control" value="<?php echo $bengkel[0]->no_telp;?>">
                        <input type="text" name="no_telp" placeholder="Masukkan No Telpon" class="form-control" value="<?php echo $bengkel[0]->no_telp;?>" onKeyPress="return goodchars(event,'0123456789.',this)" maxlength="13" required>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Jumlah Montir</label>
                        <input type="hidden" name="jumlah_montir" placeholder="Masukkan Jumlah Montir" class="form-control" maxlength="50" value="<?php echo $bengkel[0]->jumlah_montir;?>">
                        <input type="number" name="jumlah_montir" placeholder="Masukkan Jumlah Montir" class="form-control" maxlength="50" value="<?php echo $bengkel[0]->jumlah_montir;?>" required>
                        
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Alamat</label>
                        <input type="hidden" name="alamat" placeholder="Masukkan Alamat" class="form-control" maxlength="100" value="<?php echo $bengkel[0]->alamat;?>">
                        <input type="text" name="alamat" placeholder="Masukkan Alamat" class="form-control" maxlength="100" value="<?php echo $bengkel[0]->alamat;?>" required>
                        
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit</button>
                </div>
            </form>
            </div>
        </div>
    <script src="<?php echo base_url('template/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/popper.js/dist/umd/popper.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/main.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/jszip/dist/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/pdfmake/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/pdfmake/build/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.colVis.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/init-scripts/data-table/datatables-init.js');?>"></script>
</body>
</html>
