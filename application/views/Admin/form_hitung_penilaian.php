<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/bootstrap/dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/themify-icons/css/themify-icons.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/flag-icon-css/css/flag-icon.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/selectFX/css/cs-skin-elastic.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/assets/css/style.css');?>">
    <link href="<?php echo base_url('template/assets/css/font.css');?>" rel="stylesheet" type="text/css">
    <!-- Validasi dengan JavaScript -->
<script language="javascript">
	function getkey(e)
	{
		if (window.event)
   		return window.event.keyCode;
		else if (e)
   		return e.which;
		else
   		return null;
	}
	function goodchars(e, goods, field)
	{
	var key, keychar;
	key = getkey(e);
		if (key == null) return true;
		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();
		goods = goods.toLowerCase();
	// check goodkeys
	if (goods.indexOf(keychar) != -1)
			return true;
	// control keys
	if ( key==null || key==0 || key==8 || key==9 || key==27 )
		return true;		
	if (key == 13) {
			var i;
			for (i = 0; i < field.form.elements.length; i++)
					if (field == field.form.elements[i])
							break;
			i = (i + 1) % field.form.elements.length;
			field.form.elements[i].focus();
			return false;
			};
	// else return false
	return false;
	}
    </script>
</head>
<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a href="<?php echo base_url(); ?>index.php/user/admin"><br>
                <img src="<?php echo base_url('assets/spk.png');?>" style="width:180px";>
                </a>
            </div>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                <h3 class="menu-title"><center>KABUPATEN BANDUNG BARAT</center></h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Dashboard</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-calendar-check-o"></i><a href="<?php echo base_url(); ?>index.php/user/Admin">Peta Bengkel</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar"></i>Bengkel</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/bengkel/kelola_bengkel">Kelola Bengkel</a></li>
                            <!--<li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php //echo base_url(); ?>index.php/bengkel/kelola_sparepart">Kelola Sparepart</a></li>-->
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/lokasi/kelola_lokasi">Kelola Lokasi</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-sticky-note"></i>Penilaian</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_nilai_bengkel">Kelola Penilaian Bengkel</a></li>
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_hitung_bengkel">Kelola Perhitungan Bengkel</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="header-menu">
            <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                    <a class="nav-link active" href="#">Halaman <b><?php echo $this->session->userdata('username');?></b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo site_url('user/logout');?>">Logout</a>
                </li>
                </ul>
            </div>
        </header><!-- /header -->
        <!-- Header-->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Hitung Penilaian</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card">
            <form action="<?php echo base_url(); ?>index.php/maut/hitung_maut" method="POST">
                <div class="card-header">
                <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Hitung Multi Attributte Utility Theory</button>
                </div>
                    <div class="card-body card-block">
                        <h3>Penilaian</h3>
                        <?php foreach($nilai as $n) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">ID Penilaian</label>
                        <input type="hidden" name="id_penilaian[]" class="form-control" value="<?php echo $n->id_penilaian;?>" required>
                        <input type="text" name="id_penilaian[]" class="form-control" value="<?php echo $n->id_penilaian;?>"disabled >
                        </div>
                        <div class="form-group"><label for="street" class=" form-control-label">Nama bengkel</label>
                        <input type="hidden" name="nama_bengkel[]" class="form-control" value="<?php echo $n->nama_bengkel;?>" required >
                        <input type="text" name="nama_bengkel[]" class="form-control" value="<?php echo $n->nama_bengkel;?>" disabled>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Sparepart</label>
                        <input type="hidden" name="sparepart[]" class="form-control" value="<?php echo $n->sparepart;?>" required>
                        <input type="number" class="form-control" value="<?php echo $n->sparepart;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Waktu Operasional</label>
                        <input type="hidden" name="waktu_operasional[]" class="form-control" value="<?php echo $n->waktu_operasional;?>" required>
                        <input type="number"  class="form-control" value="<?php echo $n->waktu_operasional;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Fasilitas</label>
                        <input type="hidden" name="fasilitas[]" class="form-control" value="<?php echo $n->fasilitas;?>" required>
                        <input type="number" class="form-control" value="<?php echo $n->fasilitas;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Jumlah Mekanik</label>
                        <input type="hidden" name="jumlah_mekanik[]" class="form-control" value="<?php echo $n->jumlah_mekanik;?>" required>
                        <input type="number"  class="form-control" value="<?php echo $n->jumlah_mekanik;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Rating</label>
                        <input type="hidden" name="rating[]" class="form-control" value="<?php echo $n->rating;?>" required>
                        <input type="number"  class="form-control" value="<?php echo $n->rating;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Estimasi Waktu Perbaikan Ringan</label>
                        <input type="hidden" name="estimasi_waktu_ringan[]" class="form-control" value="<?php echo $n->estimasi_waktu_ringan;?>" required>
                        <input type="number"  class="form-control" value="<?php echo $n->estimasi_waktu_ringan;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Estimasi Waktu Perbaikan Sedang</label>
                        <input type="hidden" name="estimasi_waktu_sedang[]" class="form-control" value="<?php echo $n->estimasi_waktu_sedang;?>" required>
                        <input type="number"  class="form-control" value="<?php echo $n->estimasi_waktu_sedang;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Estimasi Waktu Perbaikan Berat</label>
                        <input type="hidden" name="estimasi_waktu_berat[]" class="form-control" value="<?php echo $n->estimasi_waktu_berat;?>" required>
                        <input type="number"  class="form-control" value="<?php echo $n->estimasi_waktu_berat;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Kisaran Harga Perbaikan Ringan</label>
                        <input type="hidden" name="kisaran_harga_ringan[]" class="form-control" value="<?php echo $n->kisaran_harga_ringan;?>" required>
                        <input type="number" class="form-control" value="<?php echo $n->kisaran_harga_ringan;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Kisaran Harga Perbaikan Sedang</label>
                        <input type="hidden" name="kisaran_harga_sedang[]" class="form-control" value="<?php echo $n->kisaran_harga_sedang;?>" required>
                        <input type="number" class="form-control" value="<?php echo $n->kisaran_harga_sedang;?>" >
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Kisaran Harga Perbaikan Berat</label>
                        <input type="hidden" name="kisaran_harga_berat[]" class="form-control" value="<?php echo $n->kisaran_harga_berat;?>" required>
                        <input type="number" class="form-control" value="<?php echo $n->kisaran_harga_berat;?>" >
                        </div>
                    <?php } ?>
                    <h3>Max</h3><br>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Sparepart</label>
                        <input type="hidden" name="max_sparepart" class="form-control" value="<?php echo $m->sparepart;?>" required>
                        <input type="text" name="max_sparepart" class="form-control" value="<?php echo $m->sparepart;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Waktu Operasional</label>
                        <input type="hidden" name="max_waktu_operasional" class="form-control" value="<?php echo $m->waktu_operasional;?>" required>
                        <input type="text" name="max_waktu_operasional" class="form-control" value="<?php echo $m->waktu_operasional;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Fasilitas</label>
                        <input type="hidden" name="max_fasilitas" class="form-control" value="<?php echo $m->fasilitas;?>" required>
                        <input type="text" name="max_fasilitas" class="form-control" value="<?php echo $m->fasilitas;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Jumlah Mekanik</label>
                        <input type="hidden" name="max_jumlah_mekanik" class="form-control" value="<?php echo $m->jumlah_mekanik;?>" required>
                        <input type="text" name="max_jumlah_mekanik" class="form-control" value="<?php echo $m->jumlah_mekanik;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Rating</label>
                        <input type="hidden" name="max_rating" class="form-control" value="<?php echo $m->rating;?>" required>
                        <input type="text" name="max_rating" class="form-control" value="<?php echo $m->rating;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Estimasi Waktu Ringan</label>
                        <input type="hidden" name="max_estimasi_waktu_ringan" class="form-control" value="<?php echo $m->estimasi_waktu_ringan;?>" required>
                        <input type="text" name="max_estimasi_waktu_ringan" class="form-control" value="<?php echo $m->estimasi_waktu_ringan;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Estimasi Waktu Sedang</label>
                        <input type="hidden" name="max_estimasi_waktu_sedang" class="form-control" value="<?php echo $m->estimasi_waktu_sedang;?>" required>
                        <input type="text" name="max_estimasi_waktu_sedang" class="form-control" value="<?php echo $m->estimasi_waktu_sedang;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Estimasi Waktu Berat</label>
                        <input type="hidden" name="max_estimasi_waktu_berat" class="form-control" value="<?php echo $m->estimasi_waktu_berat;?>" required>
                        <input type="text" name="max_estimasi_waktu_berat" class="form-control" value="<?php echo $m->estimasi_waktu_berat;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Kisaran Harga Ringan</label>
                        <input type="hidden" name="max_kisaran_harga_ringan" class="form-control" value="<?php echo $m->kisaran_harga_ringan;?>" required>
                        <input type="text" name="max_kisaran_harga_ringan" class="form-control" value="<?php echo $m->kisaran_harga_ringan;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Kisaran Harga Sedang</label>
                        <input type="hidden" name="max_kisaran_harga_sedang" class="form-control" value="<?php echo $m->kisaran_harga_sedang;?>" required>
                        <input type="text" name="max_kisaran_harga_sedang" class="form-control" value="<?php echo $m->kisaran_harga_sedang;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($max as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Max Kisaran Harga Berat</label>
                        <input type="hidden" name="max_kisaran_harga_berat" class="form-control" value="<?php echo $m->kisaran_harga_berat;?>" required>
                        <input type="text" name="max_kisaran_harga_berat" class="form-control" value="<?php echo $m->kisaran_harga_berat;?>" required>
                        </div>
                        <?php } ?>
                    <h3>Min</h3><br>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Sparepart</label>
                        <input type="hidden" name="min_sparepart" class="form-control" value="<?php echo $m->sparepart;?>" required>
                        <input type="text" name="min_sparepart" class="form-control" value="<?php echo $m->sparepart;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Waktu Operasional</label>
                        <input type="hidden" name="min_waktu_operasional" class="form-control" value="<?php echo $m->waktu_operasional;?>" required>
                        <input type="text" name="min_waktu_operasional" class="form-control" value="<?php echo $m->waktu_operasional;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Fasilitas</label>
                        <input type="hidden" name="min_fasilitas" class="form-control" value="<?php echo $m->fasilitas;?>" required>
                        <input type="text" name="min_fasilitas" class="form-control" value="<?php echo $m->fasilitas;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Jumlah Mekanik</label>
                        <input type="hidden" name="min_jumlah_mekanik" class="form-control" value="<?php echo $m->jumlah_mekanik;?>" required>
                        <input type="text" name="min_jumlah_mekanik" class="form-control" value="<?php echo $m->jumlah_mekanik;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Rating</label>
                        <input type="hidden" name="min_rating" class="form-control" value="<?php echo $m->rating;?>" required>
                        <input type="text" name="min_rating" class="form-control" value="<?php echo $m->rating;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Estimasi Waktu Perbaikan Ringan</label>
                        <input type="hidden" name="min_estimasi_waktu_ringan" class="form-control" value="<?php echo $m->estimasi_waktu_ringan;?>" required>
                        <input type="text" name="min_estimasi_waktu_ringan" class="form-control" value="<?php echo $m->estimasi_waktu_ringan;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Estimasi Waktu Perbaikan Sedang</label>
                        <input type="hidden" name="min_estimasi_waktu_sedang" class="form-control" value="<?php echo $m->estimasi_waktu_sedang;?>" required>
                        <input type="text" name="min_estimasi_waktu_sedang" class="form-control" value="<?php echo $m->estimasi_waktu_sedang;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Estimasi Waktu Perbaikan Berat</label>
                        <input type="hidden" name="min_estimasi_waktu_berat" class="form-control" value="<?php echo $m->estimasi_waktu_berat;?>" required>
                        <input type="text" name="min_estimasi_waktu_berat" class="form-control" value="<?php echo $m->estimasi_waktu_berat;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Kisaran Harga Ringan</label>
                        <input type="hidden" name="min_kisaran_harga_ringan" class="form-control" value="<?php echo $m->kisaran_harga_ringan;?>" required>
                        <input type="text" name="min_kisaran_harga_ringan" class="form-control" value="<?php echo $m->kisaran_harga_ringan;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Kisaran Harga Sedang</label>
                        <input type="hidden" name="min_kisaran_harga_sedang" class="form-control" value="<?php echo $m->kisaran_harga_sedang;?>" required>
                        <input type="text" name="min_kisaran_harga_sedang" class="form-control" value="<?php echo $m->kisaran_harga_sedang;?>" required>
                        </div>
                        <?php } ?>
                        <?php foreach($min as $m) { ?>
                        <div class="form-group"><label for="company" class=" form-control-label">Min Kisaran Harga Berat</label>
                        <input type="hidden" name="min_kisaran_harga_berat" class="form-control" value="<?php echo $m->kisaran_harga_berat;?>" required>
                        <input type="text" name="min_kisaran_harga_berat" class="form-control" value="<?php echo $m->kisaran_harga_berat;?>" required>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="card-footer">
                </div>
            </form>
            </div>
        </div>
    <script src="<?php echo base_url('template/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/popper.js/dist/umd/popper.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/main.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/jszip/dist/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/pdfmake/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/pdfmake/build/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.colVis.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/init-scripts/data-table/datatables-init.js');?>"></script>
</body>
</html>
