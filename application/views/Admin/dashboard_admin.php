<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Bengkel</title>
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/bootstrap/dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/themify-icons/css/themify-icons.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/flag-icon-css/css/flag-icon.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/selectFX/css/cs-skin-elastic.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/jqvmap/dist/jqvmap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/assets/css/style.css');?>">
    <link href="<?php echo base_url('template/assets/css/font.css');?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('template/vendors/chart.js/dist/Chart.js');?>"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css' rel='stylesheet' />

</head>
<body>

<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
<link
rel="stylesheet"
href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css"
type="text/css"
/>
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
<style>
#geocoder-container > div {
min-width: 50%;
margin-left: 25%;
}
.distance-container {
position: absolute;
top: 10px;
left: 10px;
z-index: 1;
}
 
.distance-container > * {
background-color: rgba(0, 0, 0, 0.5);
color: #fff;
font-size: 11px;
line-height: 18px;
display: block;
margin: 0;
padding: 5px 10px;
border-radius: 3px;
}
</style>


    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a href="<?php echo base_url(); ?>index.php/user/admin"><br>
                <img src="<?php echo base_url('assets/spk.png');?>" style="width:180px";>
                </a>
            </div>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                <h3 class="menu-title"><center>KABUPATEN BANDUNG BARAT</center></h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Dashboard</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-calendar-check-o"></i><a href="<?php echo base_url(); ?>index.php/user/Admin">Peta Bengkel</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar"></i>Bengkel</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/bengkel/kelola_bengkel">Kelola Bengkel</a></li>
                            <!--<li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php //echo base_url(); ?>index.php/bengkel/kelola_sparepart">Kelola Sparepart</a></li>-->
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/lokasi/kelola_lokasi">Kelola Lokasi</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-sticky-note"></i>Penilaian</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_nilai_bengkel">Kelola Penilaian Bengkel</a></li>
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_hitung_bengkel">Kelola Perhitungan Bengkel</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="header-menu">
            <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <ul class="nav justify-content-end">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Halaman <b><?php echo $this->session->userdata('username');?></b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo site_url('user/logout');?>">Logout</a>
                </li>
                </ul>
            </div>
        </header><!-- /header -->
        <!-- Header-->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard Admin</h1>
                    </div>
                </div>
            </div>
        </div>

        <div id='map' style='height: 840px;'></div>
        <script>
          mapboxgl.accessToken = 'pk.eyJ1Ijoic3lhcmlmdWRpbnlvZ2EiLCJhIjoiY2p1YnlrY3BpMGlzNjQ0bnBsM2swbnJ6NyJ9.iZN2GI1beXXVVoDtMAKW5g';
          
          var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            zoom: 13,
            center: [107.4833,-6.8333]
          });    
    
          var geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            marker: {
            color: 'orange'
          },
          mapboxgl: mapboxgl
        });
     
        map.addControl(geocoder);
    
    // Add geolocate control to the map.
    map.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
        })
    );

    map.on('load', function () {
            
            map.loadImage(
              'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
              // Add an image to use as a custom marker
                function (error, image) {
                  if (error) throw error;
                    map.addImage('custom-marker', image); 
                      map.addSource('places', {
                        'type': 'geojson',
                        'data': {
                        'type': 'FeatureCollection',
                        'features': [
                          <?php foreach($lokasi as $l) { ?>
                      {
                        'type': 'Feature',
                        'properties': {
                        'description':
                        '<strong><?php echo $l->nama_bengkel;?></strong>'
                        },
                          'geometry': {
                          'type': 'Point',
                          'coordinates': [<?php echo $l->longitude;?>,<?php echo $l->latitude;?>]
                        }
                      },
                      <?php } ?>
                      ]
                      }
                      });
              // Add a layer showing the places.
              map.addLayer({
                'id': 'places',
                'type': 'symbol',
                'source': 'places',
                'layout': {
                'icon-image': 'custom-marker',
                'icon-allow-overlap': true
                }
              });
              }
              )
              
              ;

              // Create a popup, but don't add it to the map yet.
              var popup = new mapboxgl.Popup({
                closeButton: false,
                closeOnClick: false
              });
 
              map.on('mouseenter', 'places', function (e) {
              // Change the cursor style as a UI indicator.
              map.getCanvas().style.cursor = 'pointer';
 
              var coordinates = e.features[0].geometry.coordinates.slice();
              var description = e.features[0].properties.description;
 
              // Ensure that if the map is zoomed out such that multiple
              // copies of the feature are visible, the popup appears
              // over the copy being pointed to.
              while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
              coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
              }
 
              // Populate the popup and set its coordinates
              // based on the feature found.
              popup.setLngLat(coordinates).setHTML(description).addTo(map);
              });
 
              map.on('mouseleave', 'places', function () {
              map.getCanvas().style.cursor = '';
              popup.remove();
              
              });
              });

              // Add zoom and rotation controls to the map.
        map.addControl(new mapboxgl.NavigationControl());
          
        </script>


           
                
    </div> <!-- .content -->
    </div><!-- /#right-panel -->
    <!-- Right Panel -->
    <script src="<?php echo base_url('template/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/popper.js/dist/umd/popper.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/main.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/chart.js/dist/Chart.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/dashboard.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/widgets.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/jqvmap/dist/jquery.vmap.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/jqvmap/dist/maps/jquery.vmap.world.js');?>"></script>
</body>
</html>
