<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/bootstrap/dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/themify-icons/css/themify-icons.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/flag-icon-css/css/flag-icon.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/selectFX/css/cs-skin-elastic.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('template/assets/css/style.css');?>">
    <link href="<?php echo base_url('template/assets/css/font.css');?>" rel="stylesheet" type="text/css">
</head>
<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a href="<?php echo base_url(); ?>index.php/user/admin"><br>
                <img src="<?php echo base_url('assets/spk.png');?>" style="width:180px";>
                </a>
            </div>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                <h3 class="menu-title"><center>KABUPATEN BANDUNG BARAT</center></h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Dashboard</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-calendar-check-o"></i><a href="<?php echo base_url(); ?>index.php/user/Admin">Peta Bengkel</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar"></i>Bengkel</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/bengkel/kelola_bengkel">Kelola Bengkel</a></li>
                            <!--<li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php //echo base_url(); ?>index.php/bengkel/kelola_sparepart">Kelola Sparepart</a></li>-->
                            <li><i class="menu-icon fa fa-calendar-plus-o"></i><a href="<?php echo base_url(); ?>index.php/lokasi/kelola_lokasi">Kelola Lokasi</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-sticky-note"></i>Penilaian</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_nilai_bengkel">Kelola Penilaian Bengkel</a></li>
                            <li><i class="fa fa-file-text"></i><a href="<?php echo base_url(); ?>index.php/maut/kelola_hitung_bengkel">Kelola Perhitungan Bengkel</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="header-menu">
            <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                    <a class="nav-link active" href="#">Halaman <b><?php echo $this->session->userdata('username');?></b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo site_url('user/logout');?>">Logout</a>
                </li>
                </ul>
            </div>
        </header><!-- /header -->
        <!-- Header-->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Data Nilai</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        <div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div>
                        <div id="notifications"><?php echo $this->session->flashdata('hps'); ?></div>
                        <div id="notifications"><?php echo $this->session->flashdata('edt'); ?></div>
                            <div class="card-header">
                                <!--<strong class="card-title"><a class="btn btn-success" href="<?php echo base_url(); ?>index.php/maut/form_tambah_penilaian">Tambah Data</a></strong>&nbsp;-->
                                <strong class="card-title"><a class="btn btn-info" href="<?php echo base_url(); ?>index.php/maut/hitung_penilaian">Hitung Penilaian</a></strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Bengkel</th>
                                            <th>Sparepart</th>
                                            <th>Waktu Operasional</th>
                                            <th>Fasilitas</th>
                                            <th>Jumlah Mekanik</th>
                                            <th>Rating</th>
                                            <th>Waktu Perbaikan Ringan</th>
                                            <th>Waktu Perbaikan Sedang</th>
                                            <th>Waktu Perbaikan Berat</th>
                                            <th>Harga Perbaikan Ringan</th>
                                            <th>Harga Perbaikan Sedang</th>
                                            <th>Harga Perbaikan Berat</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no=1;
                                    foreach($nilai as $n){ ?>
                                        <tr>
                                            <td><?php echo $no; ?></td><?php $no++; ?>
                                            <td><?php echo $n->nama_bengkel; ?></td>
                                            <td><?php echo $n->sparepart; ?></td>
                                            <td><?php echo $n->waktu_operasional; ?></td>
                                            <td><?php echo $n->fasilitas; ?></td>
                                            <td><?php echo $n->jumlah_mekanik; ?></td>
                                            <td><?php echo $n->rating; ?></td>
                                            <td><?php echo $n->estimasi_waktu_ringan; ?></td>
                                            <td><?php echo $n->estimasi_waktu_sedang; ?></td>
                                            <td><?php echo $n->estimasi_waktu_berat; ?></td>
                                            <td><?php echo $n->kisaran_harga_ringan; ?></td>
                                            <td><?php echo $n->kisaran_harga_sedang; ?></td>
                                            <td><?php echo $n->kisaran_harga_berat; ?></td>
                                            <td>
                                            <div class="row">
                                            <div class="col-sm-2">
                                            <form method="POST" action="<?php echo base_url(); ?>index.php/maut/form_lihat_penilaian">
                                                <input type="hidden" name="id_penilaian" value="<?php echo $n->id_penilaian; ?>">
                                                <input type="image" src="<?php echo base_url('template/assets/img/lihat.png');?>" style="width:20px";>
                                            </form>
                                            </div>
                                            <div class="col-sm-2">
                                            <form method="POST" action="<?php echo base_url(); ?>index.php/maut/form_edit_penilaian">
                                                <input type="hidden" name="id_penilaian" value="<?php echo $n->id_penilaian; ?>">
                                                <input type="image" src="<?php echo base_url('template/assets/img/edit.png');?>" style="width:20px";>
                                            </form>
                                            </div>
                                            
                                            </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div> <!-- .content -->
    </div><!-- /#right-panel -->
    <!-- Right Panel -->
    <script src="<?php echo base_url('template/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/popper.js/dist/umd/popper.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/main.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/jszip/dist/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/pdfmake/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/pdfmake/build/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo base_url('template/vendors/datatables.net-buttons/js/buttons.colVis.min.js');?>"></script>
    <script src="<?php echo base_url('template/assets/js/init-scripts/data-table/datatables-init.js');?>"></script>
</body>
</html>
