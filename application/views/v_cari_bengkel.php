<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
	<title>Cari Bengkel</title>
	
	
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">SPK Bengkel</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url();?>index.php/lokasi/index">Peta <span class="sr-only">(current)</span></a></li>
        <!--<li><a href="<?php //echo base_url();?>index.php/cart/index">Simulasi Belanja</a></li>-->
		<li class="active"><a href="<?php echo base_url();?>index.php/bengkel/index">Cari Bengkel</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url();?>index.php/user/index">Login</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container-fluid"><br/>
	<div class="row">
        <div class="col-md-6">
        <form action="<?php echo base_url(); ?>index.php/bengkel/cari_bengkel" method="POST">
                <div class="card-header"><h3><strong>Pilih</strong><small> Kriteria Bengkel</small></h3></div><br>
                    <div class="card-body card-block">
                        <div class="form-group"><label for="company" class=" form-control-label">Sparepart</label>
                        <select name="sparepart" class="form-control" required>
                                    <option value="" hidden>-- Pilih Kriteria Sparepart --</option>
        	                        <option value="100">Lebih Dari 100</option>
                                    <option value="66">50 – 100</option>
                                    <option value="66">Kurang Dari 50</option>
                                </select>
                                </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Waktu Operasional</label>
                        <select name="waktu_operasional" class="form-control" required>
                                <option value="" hidden>-- Pilih Penilaian Waktu Operasional --</option>
        	                    <option value="100">Lebih Dari 13 Jam</option>
                                <option value="66">9 Jam - 13 Jam</option>
                                <option value="66">Kurang Dari 9 Jam</option>
                            </select>
                            </div>
                        <div class="form-group"><label for="street" class=" form-control-label">Fasilitas</label>
                        <select name="fasilitas" class="form-control" required>
                                <option value="" hidden>-- Pilih Penilaian Fasilitas --</option>
        	                    <option value="100">Ruang Tunggu, Wifi, WC, Musholla</option>
                                <option value="75">Ruang Tunggu, Wifi, WC</option>
                                <option value="75">Ruang Tunggu, Wifi</option>
                                <option value="75">Ruang Tunggu</option>
                            </select>
                            </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Jumlah mekanik</label>
                        <select name="jumlah_mekanik" class="form-control" required>
                                <option value="" hidden>-- Pilih Penilaian Jumlah Mekanik --</option>
        	                    <option value="100">Lebih Dari 8 Orang</option>
                                <option value="66">5 Orang - 8 Orang</option>
                                <option value="66">Kurang dari 5 Orang</option>
                            </select>
                            </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Rating</label>
                          <select name="rating" class="form-control" required>
                              <option value="" hidden>-- Pilih Penilaian Rating --</option>
        	                    <option value="100">Lebih Besar Dari 4.8 Bintang</option>
                              <option value="66">4.0 Bintang - 4.5 Bintang</option>
                              <option value="66">Kurang Dari 4.0 Bintang</option>
                          </select>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Estimasi Waktu Perbaikan Ringan</label>
                          <select name="estimasi_waktu_ringan" class="form-control" required>
                              <option value="" hidden>-- Pilih Penilaian Estimasi Waktu Perbaikan --</option>
        	                    <option value="100">Perbaikan Kurang Dari 15 Menit</option>
                              <option value="66">Perbaikan 15 Menit - 60 Menit</option>
                              <option value="66">Perbaikan Lebih Dari 60 Menit</option>
                          </select>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Estimasi Waktu Perbaikan Sedang</label>
                          <select name="estimasi_waktu_sedang" class="form-control" required>
                              <option value="" hidden>-- Pilih Penilaian Estimasi Waktu Perbaikan --</option>
        	                    <option value="100">Perbaikan Kurang Dari 60 Menit</option>
                              <option value="66">Perbaikan 60 Menit - 90 Menit</option>
                              <option value="66">Perbaikan Lebih Dari 90 Menit</option>
                          </select>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Estimasi Waktu Perbaikan Berat</label>
                          <select name="estimasi_waktu_berat" class="form-control" required>
                              <option value="" hidden>-- Pilih Penilaian Estimasi Waktu Perbaikan --</option>
        	                    <option value="100">Perbaikan Kurang Dari 90 Menit</option>
                              <option value="66">Perbaikan 90 Menit - 120 Menit</option>
                              <option value="66">Perbaikan Lebih Dari 120 Menit</option>
                          </select>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Rata-Rata Harga Perbaikan Ringan</label>
                          <select name="kisaran_harga_ringan" class="form-control" required>
                              <option value="" hidden>-- Pilih Penilaian Rata-Rata Kisaran Harga --</option>
        	                    <option value="100">Kurang Dari Rp.35,000</option>
                              <option value="66">Kisaran Harga Rp.35,000 - Rp. 50,000</option>
                              <option value="66">Lebih Dari Rp. 50,000</option>
                          </select>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Rata-Rata Harga Perbaikan Sedang</label>
                          <select name="kisaran_harga_sedang" class="form-control" required>
                              <option value="" hidden>-- Pilih Penilaian Rata-Rata Kisaran Harga --</option>
        	                    <option value="100">Kurang Dari Rp.50,000</option>
                              <option value="66">Kisaran Harga Rp.50,000 - Rp. 80,000</option>
                              <option value="66">Lebih Dari Rp. 80,000</option>
                          </select>
                        </div>
                        <div class="form-group"><label for="vat" class=" form-control-label">Rata-Rata Harga Perbaikan Berat</label>
                          <select name="kisaran_harga_berat" class="form-control" required>
                              <option value="" hidden>-- Pilih Penilaian Rata-Rata Kisaran Harga --</option>
        	                    <option value="100">Kurang Dari Rp.80,000</option>
                              <option value="66">Kisaran Harga Rp.80,000 - Rp. 100,000</option>
                              <option value="66">Lebih Dari Rp. 100,000</option>
                          </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Cari</button>
                </div>
            </form>
        </div>
        <div class="col-md-6">
        <div class="card-header"><h3><strong>Hasil Pemilihan</strong><small> Kriteria Bengkel</small></h3></div><br>
            <div class="card-body card-block">
                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Nama Bengkel</th>
                            <th>Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($bengkel as $n){ ?>
                        <tr>
                            <td><?php echo $no; ?></td><?php $no++; ?>
                            <td><?php echo $n->nama_bengkel; ?></td>
                            <td><?php echo $n->alamat; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.2.3.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
</body>
</html>