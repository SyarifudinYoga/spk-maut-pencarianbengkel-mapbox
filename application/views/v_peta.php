<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
	<title>Peta</title>
	<script src='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css' rel='stylesheet'/>
    <style>
.my-icon {
  border-radius: 100%;
  width: 20px;
  height: 20px;
  text-align: center;
  line-height: 20px;
  color: white;
}

.icon-dc {
  background: #3bb2d0;
}

.icon-sf {
  background: #3bb2d0;
}

</style>
	
</head>
<body>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
<link
rel="stylesheet"
href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css"
type="text/css"
/>
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
<style>
#geocoder-container > div {
min-width: 50%;
margin-left: 25%;
}
.distance-container {
position: absolute;
top: 10px;
left: 10px;
z-index: 1;
}
 
.distance-container > * {
background-color: rgba(0, 0, 0, 0.5);
color: #fff;
font-size: 11px;
line-height: 18px;
display: block;
margin: 0;
padding: 5px 10px;
border-radius: 3px;
}
</style>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">SPK Bengkel</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url();?>index.php/lokasi/index">Peta <span class="sr-only">(current)</span></a></li>
        <!--<li><a href="<?php //echo base_url();?>index.php/cart/index">Simulasi Belanja</a></li>-->
		    <li><a href="<?php echo base_url();?>index.php/bengkel/index">Cari Bengkel</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url();?>index.php/user/index">Login</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container-fluid"><br/>
<style>
  .mapboxgl-popup {
  max-width: 400px;
  font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
}
</style>
<div id='map' style='height: 870px;'></div>
<script>
    mapboxgl.accessToken = 'pk.eyJ1Ijoic3lhcmlmdWRpbnlvZ2EiLCJhIjoiY2p1YnlrY3BpMGlzNjQ0bnBsM2swbnJ6NyJ9.iZN2GI1beXXVVoDtMAKW5g';
          
      var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        zoom: 13,
        center: [107.4833,-6.8333]
      });    

      var geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        marker: {
        color: 'orange'
      },
      mapboxgl: mapboxgl
    });
 
    map.addControl(geocoder);

// Add geolocate control to the map.
    map.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
        })
    );

          map.on('load', function () {
            
            map.loadImage(
              'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
              // Add an image to use as a custom marker
                function (error, image) {
                  if (error) throw error;
                    map.addImage('custom-marker', image); 
                      map.addSource('places', {
                        'type': 'geojson',
                        'data': {
                        'type': 'FeatureCollection',
                        'features': [
                          <?php foreach($lokasi as $l) { ?>
                      {
                        'type': 'Feature',
                        'properties': {
                        'description':
                        '<strong><?php echo $l->nama_bengkel;?></strong>'
                        },
                          'geometry': {
                          'type': 'Point',
                          'coordinates': [<?php echo $l->longitude;?>,<?php echo $l->latitude;?>]
                        }
                      },
                      <?php } ?>
                      ]
                      }
                      });
              // Add a layer showing the places.
              map.addLayer({
                'id': 'places',
                'type': 'symbol',
                'source': 'places',
                'layout': {
                'icon-image': 'custom-marker',
                'icon-allow-overlap': true
                }
              });
              }
              )
              
              ;
 
              // Create a popup, but don't add it to the map yet.
              var popup = new mapboxgl.Popup({
                closeButton: false,
                closeOnClick: false
              });
 
              map.on('mouseenter', 'places', function (e) {
              // Change the cursor style as a UI indicator.
              map.getCanvas().style.cursor = 'pointer';
 
              var coordinates = e.features[0].geometry.coordinates.slice();
              var description = e.features[0].properties.description;
 
              // Ensure that if the map is zoomed out such that multiple
              // copies of the feature are visible, the popup appears
              // over the copy being pointed to.
              while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
              coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
              }
 
              // Populate the popup and set its coordinates
              // based on the feature found.
              popup.setLngLat(coordinates).setHTML(description).addTo(map);
              });
 
              map.on('mouseleave', 'places', function () {
              map.getCanvas().style.cursor = '';
              popup.remove();
              
              });
              });

              // Add zoom and rotation controls to the map.
        map.addControl(new mapboxgl.NavigationControl());
  </script>
        
</div>

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.2.3.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
</body>
</html>