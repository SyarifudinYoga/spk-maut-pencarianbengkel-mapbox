<?php
class Bengkel_model extends CI_Model{
	function tampil_bengkel(){
        $this->db->select('*');
        $this->db->from('tbl_bengkel');
        //$this->db->join('klasifikasi_surat','klasifikasi_surat.kode_surat=surat_keluar.kode_surat');
        $result = $this->db->get();
        return $result->result();
    }

    function tambah_bengkel($table,$data){
        $this->db->insert($table,$data);
    }

    function tambah_penilaian_bengkel($table,$data){
        $this->db->insert($table,$data);
    }

    function get_nama_bengkel($where){
        $this->db->from('tbl_bengkel');
        $this->db->where('nama_bengkel', $where);
        $result = $this->db->get();
        return $result->result();
    }

    function ubah_bengkel($table,$data,$where){
        $this->db->where('nama_bengkel', $where);
        $this->db->update($table,$data);
    }

    function cari_bengkel(){
        $this->db->select('*');
        $this->db->from('tbl_penilaian');
        $this->db->join('tbl_maut','tbl_maut.nama_bengkel=tbl_penilaian.nama_bengkel');
        $this->db->join('tbl_bengkel','tbl_bengkel.nama_bengkel=tbl_penilaian.nama_bengkel');
        $this->db->where('sparepart <=',$this->input->post('sparepart'));
        $this->db->where('waktu_operasional <=',$this->input->post('waktu_operasional'));
        $this->db->where('fasilitas <=',$this->input->post('fasilitas'));
        $this->db->where('jumlah_mekanik <=',$this->input->post('jumlah_mekanik'));
        $this->db->where('rating <=',$this->input->post('rating'));
        $this->db->where('estimasi_waktu_ringan <=',$this->input->post('estimasi_waktu_ringan'));
        $this->db->where('estimasi_waktu_sedang <=',$this->input->post('estimasi_waktu_sedang'));
        $this->db->where('estimasi_waktu_berat <=',$this->input->post('estimasi_waktu_berat'));
        $this->db->where('kisaran_harga_ringan <=',$this->input->post('kisaran_harga_ringan'));
        $this->db->where('kisaran_harga_sedang <=',$this->input->post('kisaran_harga_sedang'));
        $this->db->where('kisaran_harga_berat <=',$this->input->post('kisaran_harga_berat'));
        $this->db->order_by('nilai_akhir','DESC');
        $result=$this->db->get();
        return $result->result();
    }

    function tampil_sparepart(){
        $this->db->select('*');
        $this->db->from('tbl_produk1');
        //$this->db->join('klasifikasi_surat','klasifikasi_surat.kode_surat=surat_keluar.kode_surat');
        $result = $this->db->get();
        return $result->result();
    }

    function tambah_sparepart($data){
        return $this->db->insert('tbl_produk1',$data);
    }

    function get_produk_id($where){
        $this->db->from('tbl_produk1');
        $this->db->where('produk_id', $where);
        $result = $this->db->get();
        return $result->result();
    }

    function ubah_sparepart($table,$data,$where){
        $this->db->where('produk_id', $where);
        $this->db->update($table,$data);
    }

    function hapus_sparepart($where){
        $this->db->where('produk_id', $where);
        $this->db->delete('tbl_produk1');
    }
}