<?php
class Lokasi_model extends CI_Model{
	function tampilLokasi(){
        $this->db->select('*');
        $this->db->from('tbl_lokasi');
        $result = $this->db->get();
        return $result->result();
    }

    function tampil_lokasi(){
        $this->db->select('*');
        $this->db->from('tbl_lokasi');
        $result = $this->db->get();
        return $result->result();
    }

    function tampil_bengkel(){
        $this->db->select('*');
        $this->db->from('tbl_bengkel');
        //$this->db->join('klasifikasi_surat','klasifikasi_surat.kode_surat=surat_keluar.kode_surat');
        $result = $this->db->get();
        return $result->result();
    }

    function tambah_lokasi($table,$data){
        $this->db->insert($table,$data);
    }

    function get_id_lokasi($where){
        $this->db->from('tbl_lokasi');
        $this->db->where('id_lokasi', $where);
        $result = $this->db->get();
        return $result->result();
    }

    function ubah_lokasi($table,$data,$where){
        $this->db->where('id_lokasi', $where);
        $this->db->update($table,$data);
    }
}