<?php
class User_model extends CI_Model{
	//Fungsi Validasi Login
    function validate($email,$password){
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $result = $this->db->get('tbl_user',1);
        return $result;
      }

      function tampil_lokasi(){
        $this->db->select('*');
        $this->db->from('tbl_lokasi');
        $result = $this->db->get();
        return $result->result();
    }
}