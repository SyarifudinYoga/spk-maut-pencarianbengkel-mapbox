<?php
class Maut_model extends CI_Model{
    
    function tampil_nilai(){
        $this->db->select('*');
        $this->db->from('tbl_penilaian');
        $result = $this->db->get();
        return $result->result();
    }

    function tampil_hitung(){
        $this->db->select('*');
        $this->db->from('tbl_maut');
        $this->db->order_by('nilai_akhir','DESC');
        $result = $this->db->get();
        return $result->result();
    }

    function tampil_bengkel(){
        $this->db->select('*');
        $this->db->from('tbl_bengkel');
        $result = $this->db->get();
        return $result->result();
    }

    function tampil_bengkel_penilaian(){
        $null=0;
        $this->db->select('*');
        $this->db->from('tbl_bengkel');
        $this->db->join('tbl_penilaian','tbl_penilaian.nama_bengkel=tbl_penilaian.nama_bengkel');
        $this->db->where('sparepart <=',$null);
        $this->db->where('waktu_operasional <=',$null);
        $this->db->where('fasilitas <=',$null);
        $this->db->where('jumlah_mekanik <=',$null);
        $this->db->where('rating <=',$null);
        $this->db->where('estimasi_waktu_ringan <=',$null);
        $this->db->where('estimasi_waktu_sedang <=',$null);
        $this->db->where('estimasi_waktu_berat <=',$null);
        $this->db->where('kisaran_harga_ringan <=',$null);
        $this->db->where('kisaran_harga_sedang <=',$null);
        $this->db->where('kisaran_harga_berat <=',$null);
        $result = $this->db->get();
        return $result->result();
    }

    function tambah_penilaian($table,$data){
        $this->db->insert($table,$data);
    }

    function get_id_penilaian($where){
        $this->db->from('tbl_penilaian');
        $this->db->where('id_penilaian', $where);
        $result = $this->db->get();
        return $result->result();
    }

    function edit_penilaian($table,$data,$where){
        $this->db->where('id_penilaian', $where);
        $this->db->update($table,$data);
    }

    function max(){
        $this->db->select_max('sparepart');
        $this->db->select_max('waktu_operasional');
        $this->db->select_max('fasilitas');
        $this->db->select_max('jumlah_mekanik');
        $this->db->select_max('rating');
        $this->db->select_max('estimasi_waktu_ringan');
        $this->db->select_max('estimasi_waktu_sedang');
        $this->db->select_max('estimasi_waktu_berat');
        $this->db->select_max('kisaran_harga_ringan');
        $this->db->select_max('kisaran_harga_sedang');
        $this->db->select_max('kisaran_harga_berat');
        $this->db->from('tbl_penilaian');
        $result = $this->db->get();
        return $result->result();
    }

    function min(){
        $this->db->select_min('sparepart');
        $this->db->select_min('waktu_operasional');
        $this->db->select_min('fasilitas');
        $this->db->select_min('jumlah_mekanik');
        $this->db->select_min('rating');
        $this->db->select_min('estimasi_waktu_ringan');
        $this->db->select_min('estimasi_waktu_sedang');
        $this->db->select_min('estimasi_waktu_berat');
        $this->db->select_min('kisaran_harga_ringan');
        $this->db->select_min('kisaran_harga_sedang');
        $this->db->select_min('kisaran_harga_berat');
        $this->db->from('tbl_penilaian');
        $result = $this->db->get();
        return $result->result();
    }

    function hitung_maut($table,$data){
        $this->db->insert_batch($table, $data);
    }

}